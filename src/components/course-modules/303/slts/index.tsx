import Link from "next/link";
import React from "react";

import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Box, Button, Divider, Flex, Spacer } from "@chakra-ui/react";

import Introduction from "./Introduction.mdx";

const SLTs = () => {
  return (
    <Box w="95%" marginTop="2em">
      <SLTsItems moduleTitle="Module 303" moduleNumber={303} />
      <Divider mt="5" />
      <Box py="5">
        <Introduction />
      </Box>
      <Flex direction="row">
        <Spacer />
      <Link href="/modules/303/3031">
        <Button colorScheme="green" my="1em">Get Started</Button>
      </Link>
      <Spacer />
      </Flex>
    </Box>
  );
};

export default SLTs;
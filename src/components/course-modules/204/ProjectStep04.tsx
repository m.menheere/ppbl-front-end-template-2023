import ProjectLayout from "@/src/components/lms/Lesson/ProjectLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import DocsStep04 from "@/src/components/course-modules/204/DocsStep04.mdx";

export default function ProjectStep04() {
  const slug = "project-step-04";
  const title = "Step 4: Test an Unlocking Tx"

  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <ProjectLayout moduleNumber={204} title={title} sltId="102.5" slug={slug}>
      <DocsStep04 />
    </ProjectLayout>
  );
}

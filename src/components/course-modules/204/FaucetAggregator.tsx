import { Box, Heading } from "@chakra-ui/react";
import FaucetList from "./components/FaucetList";

export default function FaucetAggregator() {
  const slug = "faucet-aggregator";

  return (
    <Box>
      <Heading>PPBL Faucet Aggregator</Heading>
      {/* <pre>{JSON.stringify(ppblContext, null, 2)}</pre> */}
      <FaucetList metadataKey="161803398875" />
    </Box>
  );
}

import {
  Box,
  Text,
  Heading,
  Stack,
  UnorderedList,
  ListItem,
  ListIcon,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Divider,
  Badge,
  useColorModeValue,
  useBreakpointValue,
} from "@chakra-ui/react";
import * as React from "react";
import { CheckCircleIcon } from "@chakra-ui/icons";

import slts from "@/src/data/slts-english.json";

import Link from "next/link";

const NarrowModuleList = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue");
  const showCategory = useBreakpointValue({ base: false, md: true });

  const showPPBL = (mid: number) => {
    let level = "";
    let color = "#fef469";

    // 100 #fef469
    // 200
    // 300 #e67359
    // 400
    if (mid < 200) {
      level = "Onboarding";
    } else if (mid < 300) {
      level = "Building Background Knowledge";
      color = "#f2c747";
    } else if (mid < 400) {
      level = "Specializing";
      color = "#e67359";
    } else {
      level = "Contributing";
      color = "theme.green";
    }

    return (
      <Box mr="5" px="1" bg={color} color="theme.dark" borderRadius="sm">
        <Text fontSize="sm">{level}</Text>
      </Box>
    );
  };

  return (
    <Box>
      <Heading size="md" pt="5" pb="3">Onboarding</Heading>
      {slts.modules
        .filter((module) => module.number < 200)
        .map((module, index: number) => (
          <Text key={index} py="2" fontSize="lg" color="yellow.300" fontWeight="bold" _hover={{ color: "theme.green" }}>
            <Link href={`/modules/${module.number}/slts`}>
              {module.number}: {module.title}
            </Link>
          </Text>
        ))}
      <Divider pt="5" />
      <Heading size="md" pt="5" pb="3">Building Background Knowledge</Heading>
      {slts.modules
        .filter((module) => module.number >= 200 && module.number < 300)
        .map((module, index: number) => (
          <Text key={index} py="2" fontSize="lg" color="orange.300" fontWeight="bold" _hover={{ color: "theme.green" }}>
            <Link href={`/modules/${module.number}/slts`}>
              {module.number}: {module.title}
            </Link>
          </Text>
        ))}
      <Divider pt="5" />
      <Heading size="md" pt="5" pb="3">Specializing + Contributing</Heading>
      <Text py="1" fontSize="lg">
      {slts.modules
        .filter((module) => module.number >= 300 && module.number < 400)
        .map((module, index: number) => (
          <Text key={index} py="2" fontSize="lg" color="orange.300" fontWeight="bold" _hover={{ color: "theme.green" }}>
            <Link href={`/modules/${module.number}/slts`}>
              {module.number}: {module.title}
            </Link>
          </Text>
        ))}
      </Text>
    </Box>
  );
};

export default NarrowModuleList;

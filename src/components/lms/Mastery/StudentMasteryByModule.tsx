import { Box, Text, Heading, useColorModeValue, Grid } from "@chakra-ui/react";
import * as React from "react";
import slts from "@/src/data/slts-english.json";
import { PPBLContext } from "@/src/context/PPBLContext";
import { hexToString } from "@/src/utils";
// import slts from "@/src/data/slts-indonesian.json";

interface SLT {
  id: string;
  slt: string;
}

type Props = {
  moduleNumber: number;
  id: string;
};

const StudentMasteryByModule = () => {
  const ppblContext = React.useContext(PPBLContext);

  return (
    <Box>
      <Heading>Mastery By Module</Heading>

      <Grid templateColumns={{ base: "repeat(1, 1fr)", lg: "repeat(2, 1fr)" }} gap={4}>
        {slts.modules
          .filter((module) => module.number < 300)
          .map((module, index: number) => (
            <>
              {ppblContext.completedModules.includes(module.number) ? (
                <Box key={index} p="1" bg="theme.green" color="theme.dark">
                  <Text fontSize="xl">
                    {module.number}: {module.title}
                  </Text>
                </Box>
              ) : (
                <Box key={index} p="1" bg="theme.yellow" color="theme.dark">
                  <Text fontSize="xl">
                    {module.number}: {module.title}
                  </Text>
                </Box>
              )}
            </>
          ))}
      </Grid>
    </Box>
  );
};

export default StudentMasteryByModule;

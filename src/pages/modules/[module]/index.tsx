import { useRouter } from "next/router";
import Head from "next/head";
import React from "react";

import Sidebar from "@/src/components/ui/Text/Sidebar";
import slt from "@/src/data/slts-english.json";

const Module = () => {
  const router = useRouter();
  const { module } = router.query;
  let moduleId = module ? parseInt(module?.toString()) : 0;

  const moduleSelected = slt.modules.find((m) => m.number === moduleId);

  return (
    <>
      <Head>
        <title>PPBL Module {module}</title>
      </Head>
      <div>a
        <Sidebar
          items={moduleSelected?.lessons ?? []}
          modulePath={`/modules/${module}`}
          selected={0}
        />
      </div>
    </>
  );
};

export default Module;
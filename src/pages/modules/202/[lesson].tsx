import SLTs202 from "@/src/components/course-modules/202/202-SLTs";
import Lesson2021 from "@/src/components/course-modules/202/Lesson-2021";
import Lesson2022 from "@/src/components/course-modules/202/Lesson-2022";
import Lesson2023 from "@/src/components/course-modules/202/Lesson-2023";
import Lesson2024 from "@/src/components/course-modules/202/Lesson-2024";
import Lesson2025 from "@/src/components/course-modules/202/Lesson-2025";
import Assignment2021 from "@/src/components/course-modules/202/Assignment2021";
import Summary202 from "@/src/components/course-modules/202/Summary202";
import Status202 from "@/src/components/course-modules/202/Status202";
import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import slt from "@/src/data/slts-english.json";

const Module202Lessons = () => {
  const moduleSelected = slt.modules.find((m) => m.number === 202);

  const status = null;

  // Sidebar items are generated from module.lessons i.e. from the JSON file
  // Here we simply set the contents by matching the slug and key
  const lessons = [
    { key: "slts", component: <SLTs202 /> },
    { key: "2021", component: <Lesson2021 /> },
    { key: "2022", component: <Lesson2022 /> },
    { key: "2023", component: <Lesson2023 /> },
    { key: "2024", component: <Lesson2024 /> },
    { key: "2025", component: <Lesson2025 /> },
    { key: "assignment2021", component: <Assignment2021 /> },
    { key: "summary", component: <Summary202 /> },
  ];

  return (
    <ModuleLessons
      items={moduleSelected?.lessons ?? []}
      modulePath="/modules/202"
      selected={0}
      lessons={lessons}
      status={<Status202 />}
    />
  );
};

export default Module202Lessons;

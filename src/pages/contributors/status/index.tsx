import GPTENav from "@/src/components/gpte/GPTENav";
import { CourseMasteryOverview } from "@/src/components/lms/Mastery/CourseMasteryOverview";
import { PPBLContext } from "@/src/context/PPBLContext";
import { Box, Heading, Button, Text, Input, Spacer, Flex } from "@chakra-ui/react";
import Head from "next/head";
import { useContext, useEffect, useState } from "react";

export default function ManagePage() {
  const ppblContext = useContext(PPBLContext);

  return (
    <>
      <Head>
        <title>Course Status</title>
        <meta name="description" content="PPBL Manage Treasury" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <GPTENav />
      <Box w="90%" mx="auto" my="5">
        <Heading>Demo: PPBL 2023 Course Status</Heading>
        <CourseMasteryOverview />
      </Box>
    </>
  );
}
